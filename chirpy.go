package main

import (
	"net/http"

	"github.com/go-chi/chi"
)

func main() {
	server := http.Server{
		Addr:    "localhost:8080",
		Handler: root(),
	}
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func root() http.Handler {
	r := chi.NewRouter()
	r.Use(middlewareCors)

	metrics := &apiConfig{}

	fsHandler := metrics.middlewareMetricsInc(http.StripPrefix("/app", http.FileServer(http.Dir("."))))
	r.Get("/app/*", fsHandler)
	r.Get("/app", fsHandler)

	// API endpoints
	api := chi.NewRouter()
	api.Get("/healthz", healthz())
	api.Get("/metrics", metrics.serve())
	api.Get("/reset", metrics.reset())
	r.Mount("/api", api)

	// Admin portal
	admin := chi.NewRouter()
	admin.Get("/metrics", metrics.admin())
	r.Mount("/admin", admin)

	return r
}

func middlewareCors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}
		next.ServeHTTP(w, r)
	})
}
