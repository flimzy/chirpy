package main

import (
	"bytes"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestIndex(t *testing.T) {
	s := httptest.NewServer(root())
	t.Cleanup(s.Close)

	req, err := http.NewRequest(http.MethodGet, s.URL+"/app/index.html", nil)
	if err != nil {
		t.Fatal(err)
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected 200, got %d", res.StatusCode)
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Contains(body, []byte("Welcome to Chirpy")) {
		t.Errorf("Body does not contain expected content")
	}
}

func TestHealthz(t *testing.T) {
	s := httptest.NewServer(root())
	t.Cleanup(s.Close)

	req, err := http.NewRequest(http.MethodGet, s.URL+"/api/healthz", nil)
	if err != nil {
		t.Fatal(err)
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected 200, got %d", res.StatusCode)
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Contains(body, []byte("OK")) {
		t.Errorf("Body does not contain expected content")
	}
}

func Test_fileserverHits(t *testing.T) {
	metrics := &apiConfig{}
	h := http.HandlerFunc(func(http.ResponseWriter, *http.Request) {})

	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/", nil)

	metrics.middlewareMetricsInc(h).ServeHTTP(rec, req)

	_ = rec.Result().Body.Close()

	if metrics.fileserverHits != 1 {
		t.Errorf("Unexpected number of hits recorded: %d", metrics.fileserverHits)
	}
}
